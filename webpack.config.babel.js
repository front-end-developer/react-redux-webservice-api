import {resolve} from 'path';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackUglifyJsPlugin from 'webpack-uglify-js-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import 'babel-polyfill';

const webPackSettings = (env) => {
    //if (process.env.NODE_ENV === 'development') {
    console.log('NODE_ENV: ', env.NODE_ENV);
    console.log('Production: ', env.production);
    console.log('env.prod: ', env.prod);
    console.log('env.dev: ', env.dev);
    //}
    const webpackConfig = {
        context: __dirname,
        entry: {
            vendors: ["jquery", "underscore"/* , "popper" , "bootstrap" */],
            app: [
                'babel-polyfill',
                './src/js/app.js'
            ]
        },
        target: 'web',
        output: {
            crossOriginLoading: "anonymous",
            path: resolve(__dirname + '/build'),
            filename: 'bundle.[name].min.js',
            sourceMapFilename: '[name].map',
            publicPath: ''
        },
        devServer: {
            publicPath: resolve('/build/'),
            historyApiFallback: true
        },
        devtool: 'source-map', // env.prod ? 'source-map' : 'eval', // 'cheap-module-source-map',
        resolve: {
            extensions: ['.js', '.jsx', '.ejs', '.json', '.html'],

            // Replace moduls with other modules or paths for compatiblility or convenience
            alias: {
                'React': 'react/addons',
                'jquery': 'jquery',
                'underscore': 'Underscore'//,
                //'popper': 'popper' //,
                //'bootstrap': 'bootstrap'
            }
        },

        stats: {
            colors: true,
            reasons: true,
            chunks: true
        },

        module: {
            rules: [
                {
                    enforce: 'pre',  // preloader for webpack 2, means run this loader before you run the build process
                    test: /\.js$/,
                    loaders: ['babel-loader', 'eslint-loader'],
                    //loader: 'eslint-loader',
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /\.ejs/,
                    use: [{
                        loader: 'ejs-loader'
                    }],
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /\.ts$/,
                    loader: 'awesome-typescript-loader',
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /\.json$/,
                    use: [{
                        loader: 'json-loader'
                    }],
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /jquery\.js$/,
                    use: [{
                        loader: 'expose-loader',
                        options: '$'
                    }, {
                        loader: 'expose-loader',
                        options: 'jQuery'
                    }]
                }, {
                    test: /underscore/,
                    use: [{
                        loader: 'expose-loader',
                        options: '_'
                    }]
                }, {
                    test: /bootstrap/,
                    use: [{
                        loader: 'expose-loader',
                        options: 'bootstrap'
                    }]
                }, {
                    test: /\.js$/,
                    loaders: ['babel-loader'],
                    exclude: [/node_modules/, /lib/]
                }, {
                    test: /\.jsx$/,
                    enforce: "pre",
                    loaders: ['babel-loader'],
                    exclude: [/node_modules/, /lib/]
                }, {
                    test: /\.scss$/,
                    //loaders: ['style-loader', 'css-loader', 'autoprefixer-loader', 'sass-loader']
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    // If you are having trouble with urls not resolving add this setting.
                                    // See https://github.com/webpack-contrib/css-loader#url
                                    url: false,
                                    minimize: true,
                                    sourceMap: true
                                }
                            },
                            {
                                loader: 'autoprefixer-loader'
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true,
                                    includePaths: [
                                        resolve(__dirname + 'main.scss')
                                    ]
                                }
                            }]
                    })
                },
                {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    url: false
                                }
                            },
                            {
                                loader: 'autoprefixer-loader'
                            }
                        ]
                    })
                    //loaders: ['style-loader', 'css-loader', 'autoprefixer-loader']
                }
            ]
        },

        // Plugins are responsible for taking a module and making a file out of them
        plugins: [
            new ExtractTextPlugin('styles.[name].min.css'),

            new WebpackUglifyJsPlugin({
                cacheFolder: resolve(__dirname, 'build/'),
                debug: true,
                minimize: true,
                sourceMap: true,
                output: {
                    comments: false
                },
                compressor: {
                    warnings: false,
                    keep_fnames: true
                },
                mangle: {
                    keep_fnames: true
                }
            })
            /*
            new HtmlWebpackPlugin({
                template: './index.html',
                title: 'b-startups',
                inject: 'body'
            })*///,
            // new webpack.optimize.CommonsChunkPlugin({name: 'vendor', filename: 'vendor.bundle.js'}),
            //new webpack.HotModuleReplacementPlugin()
        ]
    }

    console.log('NODE ENVIRONMENT: ', process.env.NODE_ENV);
    if (process.env.NODE_ENV === 'production') {
        //webpackConfig.devtool = false;
        /*
        webpackConfig.plugins.push([
            new WebpackUglifyJsPlugin({
                cacheFolder: resolve(__dirname, 'build/'),
                debug: false,
                minimize: true,
                sourceMap: false,
                output: {
                    comments: false
                },
                compressor: {
                    warnings: false,
                    keep_fnames: true
                },
                mangle: {
                     keep_fnames: false,
                     sequences: true,
                     dead_code: true,
                     conditionals: true,
                     booleans: true,
                     unused: true,
                     if_return: true,
                     join_vars: true,
                     drop_console: true
                },
                 uglifyOptions: {
                     compress: {
                        drop_console: true
                     }
                 }
            })
        ]);
        */
        // webpackConfig.
    }

    return webpackConfig;
}

module.exports = webPackSettings;