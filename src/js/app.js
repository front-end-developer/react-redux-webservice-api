/**
 * Created by Mark Webley.
 *
 * test urls going to:
 * http://localhost:8081/home
 * http://localhost:8081/#/home
 */
import React from 'react';
import { render } from 'react-dom';
import { HashRouter, Switch, Route, Link, BrowserRouter } from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
// import allReducers from './reducers/allReducers';
// import Header from './components/common/header/index';
import { Main } from './components/common/main/index';
// import Storage from './services/api/storage';
import './main.scss';

const App = () => (
    <div> SIMPLE TESTING

    </div>
)

const store = createStore(allReducers);

render((
    <BrowserRouter>
        <App />
    </BrowserRouter>
), document.getElementById('app'));
