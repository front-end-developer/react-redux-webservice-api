/**
 * Created by Mark Webley on 03/08/2018.
 */

// import Rx from 'rxjs/Rx';
import React from 'react';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import {pageStorage} from '../../actions/store/pageStorage';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class Storage extends React.Component {
    constructor(props) {
        super(props);
        this.showcaseAPIUrl = '../jsontest/pages.json';
        this.testAPIUrl = 'https://swapi.co/api/people';
        // this.showcaseAPIUrl = 'http://localhost:8080/webservices/jsontest/pages.json';
        this.dataObject = {
            'home': null,
            'about': null,
            'consulting': null,
            'services': null,
            'contact': null
        }
        this.state = {
            pageStorage: {}
        }
        // this.getAll();
    }

    observerFunc() {
        return {
            next: x => {
                console.log('Storage Observer got a next value: ' + x);
                this.loading = false;
                console.log('results:', JSON.stringify(x, null, 7));
                let homeData = x.filter((data) => {
                    return Object.keys(data).toString() === "home";
                });
                let aboutData = x.filter((data) => {
                    return Object.keys(data).toString() === "about";
                });
                let consultingData = x.filter((data) => {
                    return Object.keys(data).toString() === "consulting";
                });
                let servicesData = x.filter((data) => {
                    return Object.keys(data).toString() === "services";
                });
                let contactData = x.filter((data) => {
                    return Object.keys(data).toString() === "contact";
                });
                if (servicesData.length) {
                    this.dataObject  = {
                        'home': homeData[0],
                        'about': aboutData[0],
                        'consulting': consultingData[0],
                        'services': servicesData[0],
                        'contact': contactData[0]
                    };
                    //this.props.pageStorage('test page stroe');
                    //this.setState({pageStorage: 'test page stroe'});
                    return this.dataObject;
                }
            },
            error: err => console.error('Observer got an error: ' + err),
            complete: () => console.log('Observer got a complete notification')
        };
    }

    getAll() {
        let observer = this.observerFunc();
        return this.getShowCase().subscribe(observer);
    }

    getShowCaseById(section, id) {
        let dataFilter = null;
        if (typeof this.dataObject !== 'undefined') {
            dataFilter = this.dataObject[section][section].filter((page) => page.id == id);
        }
        return dataFilter;
    }

    getShowCaseByKeywords(section, keywords) {
        debugger;
        let dataFilter = null;
        if (typeof this.dataObject !== 'undefined') {
            dataFilter = this.dataObject[section][section].filter((page) => {
                return page.en.service.toLowerCase().includes(keywords.toLowerCase());
            });
        }
        return dataFilter;
    }

    getShowCaseByName(section) {
        let dataFilter = null;
        if (typeof this.payLoad !== 'undefined') {
            dataFilter = this.payLoad.map((sectionName, i) => {
                if (Object.keys(sectionName).toString() === section) {
                    return sectionName[section];
                }
            });
        }
        return dataFilter;
    }

    getShowCase() {
                                                    // this.testAPIUrl, this.showcaseAPIUrl
        let observable = Observable.fromPromise(fetch(this.testAPIUrl).then(response => response.json()));
        return observable;
    }


}

/*
function matchDispatchToProps(dispatch) {
    return bindActionCreators({pageStorage: pageStorage}, dispatch);
}
*/

function matchDispatchToProps(dispatch) {
    return bindActionCreators({pageStorage: pageStorage}, dispatch);
}

// activatedConsultancyService
exports.Storage = connect(null, matchDispatchToProps)(Storage);
//exports.Storage = connect(matchDispatchToProps)(Storage);
export default Storage;