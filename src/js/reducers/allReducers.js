/**
 * Created by Mark on 2018.
 */
import {combineReducers} from 'redux';
import UserReducers from './reducer-users'
import ActivatedConsultancyService from './activatedConsultancyService';
import pageStorage from './pageStorage';

const allReducers = combineReducers({
    users: UserReducers,
    activatedConsultancyService: ActivatedConsultancyService,
    pageStorage: pageStorage
});
export default allReducers;
