/**
 * Created by Mark on 2018.
 */
export default function() {
    return [
        {
            id: 1,
            sesisonId: '',
            firstName: "Mark",
            lastName: "Webley",
            userType: "Admin",
            avatar: ""
        }, {
            id: 2,
            sesisonId: '',
            firstName: "Recruiter",
            lastName: "Agent",
            userType: "Recruiter",
            avatar: ""
        }, {
            id: 3,
            sesisonId: '',
            firstName: "Client",
            lastName: "client",
            userType: "Direct Client",
            avatar: ""
        }, {
            id: 4,
            sesisonId: '',
            firstName: "Propect",
            lastName: "prospective client",
            userType: "Direct Client - for conversion into a client",
            avatar: ""
        }
    ]
}
