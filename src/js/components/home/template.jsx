import React from 'react';
import StringUtils from '../../com/utils/StringUtils';
import PanelDouble from '../../components/common/panels/double/index';
export default (home) => {
    debugger;
    home = home['home'];
    return (
        /* TODO:
            as a map
         */
        <div className="home">
            <div className="container">
                <div className="row mt-3">
                    <PanelDouble
                        identifier={'home-' + StringUtils.guid()}
                        title={home[0].en.enTitle}
                        summary={home[0].en.enSummary}
                        description={home[0].en.enDescription}
                        links={home[0].en.links}
                        images={home[0].images}
                        video={home[0].videos}
                        data={home[0].date}
                        background={home[0].backgroundImages[0].file}
                        addClass="p-medium"
                    />

                </div>
            </div>
        </div>
    );
};