/**
 * Created by Mark Webley 2018.
 */
import React, { PureComponent } from 'react';
import Storage from '../../services/api/storage';
import {Animated} from "react-animated-css";
import Template from './template';
// TODO: find a way to auto compile the ./style.scss and save the ./style.css
import './style.scss';


// TODO temp until class is done
const PageDao = function(page) {
    // set defaults
    // TODO: create a class for this
    let dummy = {[page]: []};
    for (let i = 0; i < 20; i++) {
        dummy[page][i] = {};
        dummy[page][i].en = {};
        dummy[page][i].en.enTitle = '';
        dummy[page][i].en.enSummary = '';
        dummy[page][i].en.enDescription = '';
        dummy[page][i].en.links = '';
        dummy[page][i].images = '';
        dummy[page][i].videos = '';
        dummy[page][i].date = '';
        dummy[page][i].backgroundImages = [];
        dummy[page][i].backgroundImages[0] = {};
        dummy[page][i].backgroundImages[0].file = '';
        dummy[page][i].backgroundImages[1] = {};
        dummy[page][i].backgroundImages[1].file = '';
        dummy[page][i].backgroundImages[2] = {};
        dummy[page][i].backgroundImages[2].file = '';
        dummy[page][i].backgroundImages[3] = {};
        dummy[page][i].backgroundImages[3].file = '';
        dummy[page][i].backgroundImages[4] = {};
        dummy[page][i].backgroundImages[4].file = '';
    }
    return dummy;
}

// TODO: read https://reactjs.org/docs/react-api.html#react.purecomponent
class Home extends React.Component {
// class Home extends PureComponent {
    constructor (props) {
        super(props);
        this.state = {
            'pageData': null
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick () {
        console.log('handle click - clicked');
    }

    componentDidMount() {
        let observer = {
            next: x => {
                console.log('Observer got a next value: ' + x);
                debugger;
                this.loading = false;
                console.log('results:', JSON.stringify(x, null, 7));
                let pageData = x.filter((data) => {
                    return Object.keys(data).toString() === "home";
                });
                if (pageData.length) {
                    this.setState({
                        'pageData': pageData[0]
                    });
                }
            },
            error: err => console.error('Observer got an error: ' + err),
            complete: () => console.log('Observer got a complete notification')
        };
        new Storage().getShowCase().subscribe(observer);
    }

    render() {
        const {home} = this.state.pageData || PageDao('home');
        debugger;
        return (
            <Template home={home} />
        )
    }
}

export default Home;
