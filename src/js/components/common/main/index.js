/**
 * Created by Mark Webley 2018.
 */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../../home/index';

// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /details
// and /schedule routes will match any pathname that starts
// with /details or /schedule. The / route will only match
// when the pathname is exactly the string "/"
const Main = () => (
    <main>
        <Switch>
            <Route exact path='/' component={Home}/>
        </Switch>
    </main>
)
exports.Main = Main;
exports.ConsultingViews = ConsultingViews;