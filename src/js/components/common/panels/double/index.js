/**
 * Created by Mark 2018.
 */
import React, { Component, PropTypes } from 'react';
import {Link} from 'react-router-dom';
import {Animated} from "react-animated-css";
import styles from './style.scss';
class PanelDouble extends Component {
    /*
    propTypes = {
        title: PropTypes.string.isRequired
    }
    */
    constructor(prop) {
        super(prop);
        /*
        this.state = {
            title: null,
            summary: null,
            description: null,
            identifier: null
        };
        */
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick () {
        console.log('handle click - clicked');
    }
    componentDidMount() {
        console.log('props', this.props);
        // todo: test, get export woring with sass or css variable :export ...and use this if above is not supported
        console.log('styles: ', styles.newPanelBackground);
    }
    render() {
        console.log('props', this.props);
        const {
            identifier,
            title,
            summary,
            description,
            links,
            images,
            video,
            data,
            background
        } = this.props;
        let classString = this.props.background ? 'col-12 col-sm-8 col-md-6 panel-double background' : 'col-12 col-sm-8 col-md-6 panel-double';
        if (this.props.addClass) {
            classString = classString.concat(' ', this.props.addClass);
        }
        if (this.props.identifier) {
            classString = classString.concat(' ', this.props.identifier);
        }

        if (typeof this.props.identifier != 'undefined' && this.props.background.length > 5) {
            // change background image using css var
            const selectorElement = ''.concat('div[class*="', this.props.identifier, '"]');
            const newBg = ''.concat('url("', this.props.background, '") no-repeat');
            let domChecker = setInterval(() => {
                if (document.querySelector(selectorElement) !== null || domChecker > 50) {
                    document.querySelector(selectorElement).style.setProperty('--panel-double-background', newBg);
                    console.log('dom CHEcKCER:::', domChecker);
                    clearInterval(domChecker);
                }
            });
        }
        const hrefLink = (links.length == 0) ? '' : <Link to={links[0].link}>{links[0].title}</Link>;
        return (
            <Animated className={classString} animationIn={this.props.animationIn} animationInDelay={this.props.animationInDelay} isVisible={true}>
                <h5>{title}</h5>
                <p>
                    <span onClick={this.handleClick}>{summary}</span> {hrefLink}
                </p>
            </Animated>
        )
    }
}

PanelDouble.defaultProps = {
    identifier: '', // todo set default dynamic value
    background: '',
    animationIn: 'fadeInDown',
    animationInDelay: 1500,
    addClass: '',
    links: [{link: '', title: ''}]
}

export default PanelDouble;