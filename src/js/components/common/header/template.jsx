import React from 'react';
import { Link } from 'react-router-dom';
export default (data) => {
    debugger;
    const context = data.context;
    return (
        <header>
            <div className="navbar navbar-default">
                <nav className="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
                    <a className="navbar-brand" href="#">
                        <span className="logo">b-startups</span>
                        <span className="mark-webley">A Mark Webley Initiative</span>
                    </a>
                    <button onClick={context.toggleNavigation} className="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div onClick={context.toggleMenuButton} className="navbar-collapse collapse offcanvas-collapse">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item"><Link className="nav-link" to='/'>Home</Link></li>
                            <li className="nav-item"><Link className="nav-link" to='/about'>About</Link></li>
                            <li className="nav-item"><Link className="nav-link" to='/services'>Services</Link></li>
                            <li className="nav-item"><Link className="nav-link" to='/consulting'>Consulting</Link></li>
                            <li className="nav-item"><Link className="nav-link" to='/investors'>Investors</Link></li>
                            <li className="nav-item"><Link className="nav-link" to='/contacts'>Contacts</Link></li>
                        </ul>

                        { /*
                        <div className="form-inline my-2 my-lg-0">
                            <div onClick={context.languageChange} onMouseOver={context.languageToggle} onMouseOut={context.languageToggle} className="language-flags mr-sm-2" id="exampleSelect1">
                                <span className="flags-en active">EN</span>
                                <span className="flags-es">ES</span>
                            </div>
                        </div>

                        <form className="form-inline my-2 my-lg-0">
                            <input className="form-control mr-sm-2 search" type="text" placeholder="Search" aria-label="Search" />
                            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                            */ }
                    </div>
                </nav>
            </div>
        </header>
    )
}