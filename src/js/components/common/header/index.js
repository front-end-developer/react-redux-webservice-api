/**
 * Created by Mark 2018.
 */
import React, { PropTypes } from 'react';
import Template from './template';
import 'bootstrap';
import './style.scss'

// The Header creates links that can be used to navigate
// between routes.
class Header extends React.Component {
    constructor (props) {
        super(props);
        this.toggleNavigation = this.toggleNavigation.bind(this, event);
    }

    toggleNavigation(e) {
        debugger;
        document.querySelector('.navbar-collapse').classList.toggle('collapse');
        document.querySelector('nav').classList.toggle('border-effect');

        /*
         document.querySelectorAll('.dropdown-menu').forEach((item, i) => {
         item.classList.remove('show');
         });
         const node = e.target.parentElement.nextElementSibling;
         if (node.classList.contains('dropdown-menu')) {
         (node.classList.contains('show'))
         ? node.classList.remove('show')
         : node.classList.add('show');
         }
         */
    }

    toggleMenuButton(e){
        debugger;
        e.preventDefault();
        const navNode = document.querySelector('nav');
        document.querySelector('.navbar-collapse').classList.toggle('collapse');
        if (navNode.classList.contains('border-effect')){
            navNode.classList.remove('border-effect');
        }
    }

    render () {
        return (
            <Template context={this} />
        )
    }
}

export default Header;