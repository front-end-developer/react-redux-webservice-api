/**
 * Created by WebEntra on 04/04/2018.
 */
export const selectConsultingService = (consultingService) => {
    console.log('you clicked on selectConsultingService: ', JSON.stringify(consultingService, null, 7) );
    return {
        type: 'CONSULTING_SERVICE_SELECTED',
        data: consultingService
    }
}